package Ejercicio_Games;
import java.util.Optional;

public class PrincipalGame {
	static String gameName="JuegoNuevo";
	static double playerPosx=0;
	static double playerPosY=0;
	public static void main(String[] args){
		PrincipalGame p = new PrincipalGame();
		//Con lambda más de un parámetro
		Playeable w = (pPosX, pPosY)-> {
			playerPosx+=pPosX;
			playerPosY+=pPosY;
		};

		w.walk(1,1);
		w.walk(1,1);
        System.out.println("Pos [" + playerPosx + "],[" + playerPosY + "]");
		Gameable g = ()-> {
			Game j = new Game();
			j.setGameName(gameName);
		};

		g.startGame();

		Soundable s = (Song) -> {
			Optional<String> cancion = Optional.of(Song);
			cancion.orElse("OtroNombre");
			System.out.println(cancion);
		};

		s.playMusic("Cancion");

	}
}