package Ejercicio_Games;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
public class Alumno {
	String nombre;
	String apellidoPaterno;
	String apellidoMaterno;
	String nombreCurso;

	public void setNombre(String nombre){
		this.nombre=nombre;
	}

	public String getNombre(){
		return nombre;
	}

	public void setApPaterno(String ApPaterno){
		this.apellidoPaterno=ApPaterno;
	}

	public String getApPaterno(){
		return apellidoPaterno;
	}

	public void setApMaterno(String ApMaterno){
		this.apellidoMaterno=ApMaterno;
	}

	public String getApMaterno(){
		return apellidoMaterno;
	}

	public void setNombreCurso(String nombreCurso){
		this.nombreCurso=nombreCurso;
	}

	public String getNombreCurso(){
		return nombreCurso;
	}

	@Override
	public String toString(){
		return " Nombre: "+ nombre
			+"\n ApPaterno: "+ apellidoPaterno
			+"\n ApMaterno: "+ apellidoMaterno
			+"\n nombreCurso: "+nombreCurso;
	}
	public static void main(String[] args){
		List<Alumno> alumno = new ArrayList<Alumno>();
		for(int i=0;i<20;i++){
			Alumno al =  new Alumno();
			al.setNombre("Nombre"+i+(i%2==0?"a":"b"));
			al.setApPaterno("ApPaterno"+i);
			al.setApMaterno("ApMaterno"+i);
			al.setNombreCurso("Nombre Curso"+i);
			alumno.add(al);
		}
		System.out.println("Todos los elementos ");
		alumno.stream()
		.forEach(System.out::println);
		System.out.println("Longitud lista: "+alumno.stream().count());
		System.out.println("Todos los elementos que terminan con a");
		alumno
		.stream()
		.filter(x -> x.nombre.endsWith("a") )
		.collect(Collectors.toList())
		.forEach(System.out::println);
		System.out.println("Primeros 5 elementos");
		alumno
		.stream()
		.limit(5)
		.collect(Collectors.toList())
		.forEach(System.out::println);
		
	}
}