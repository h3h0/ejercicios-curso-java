package mx.com.praxis.services;

import java.util.ArrayList;
import java.util.List;

import mx.com.praxis.daos.AutomovilDao;
import mx.com.praxis.dtos.AutomovilDto;
import mx.com.praxis.entities.Automovil;

public class AutomovilServiceImpl implements AutomovilService {
	
	public void update(AutomovilDto auto) {
		// TODO Auto-generated method stub

	}

	public void insert(AutomovilDto auto) {
		AutomovilDao autoDao = AutomovilDao.getInstance();
		Automovil aux = new Automovil();
		aux.setPlacas(auto.getPlacas());
		aux.setMarca(auto.getMarca());
		aux.setTipoCombustible(auto.getTipoCombustible());
		aux.setModelo(auto.getModelo());
		autoDao.insert(aux);
		// TODO Auto-generated method stub

	}

	public List<AutomovilDto> getAutos() {
		AutomovilDao autoDao = AutomovilDao.getInstance();
		List<AutomovilDto> autosDto = new ArrayList();
		for(Automovil auto:autoDao.findAll()){
			AutomovilDto aux = new AutomovilDto();
			aux.setPlacas(auto.getPlacas());
			aux.setMarca(auto.getMarca());
			aux.setTipoCombustible(auto.getTipoCombustible());
			aux.setModelo(auto.getModelo());
			autosDto.add(aux);
		}
		return autosDto;
	}

	public AutomovilDto findById(String placas) {
		// TODO Auto-generated method stub
		return null;
	}

}
