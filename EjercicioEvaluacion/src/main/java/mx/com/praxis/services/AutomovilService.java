/*
 * Copyright (c) 3/18/20
 * Mexico D.F.
 * All rights reserved.
 *
 * THIS SOFTWARE IS  CONFIDENTIAL INFORMATION PROPRIETARY OF
 * THIS INFORMATION SHOULD NOT BE DISCLOSED AND MAY ONLY BE USED IN ACCORDANCE THE TERMS DETERMINED BY THE COMPANY ITSELF.
 */
package mx.com.praxis.services;

import java.util.List;

import mx.com.praxis.dtos.AutomovilDto;

/**
 * <p>
 * </p>
 *
 * @author Marco Acevedo
 * @version Programming
 * @since Programming
 */
public interface AutomovilService {

	List<AutomovilDto> getAutos();

	AutomovilDto findById(String placas);

	void update(AutomovilDto auto);

	void insert(AutomovilDto auto);

	static AutomovilService getInstance() {
		return new AutomovilServiceImpl();
	}
}
