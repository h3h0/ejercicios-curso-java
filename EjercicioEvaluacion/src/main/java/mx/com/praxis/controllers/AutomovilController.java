package mx.com.praxis.controllers;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.praxis.dtos.AutomovilDto;
import mx.com.praxis.services.AutomovilService;

@WebServlet("/automovil")
public class AutomovilController extends HttpServlet {
	private static final long serialVersionUID = 4231814006439125033L;

	AutomovilService automovilservice = AutomovilService.getInstance();
	
	@Override
	protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {

		AutomovilDto auto = new AutomovilDto();
		auto.setPlacas(request.getParameter("placas"));
		auto.setMarca(request.getParameter("marca"));
		auto.setTipoCombustible(request.getParameter("tipoCombustible"));
		auto.setModelo(Integer.parseInt(request.getParameter("modelo")));
		automovilservice.insert(auto);
		response.sendRedirect(request.getContextPath() + "/automovil");
	}

	@Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("autos", automovilservice.getAutos());
		request.getRequestDispatcher("autos.jsp").forward(request,response);
	}
}
