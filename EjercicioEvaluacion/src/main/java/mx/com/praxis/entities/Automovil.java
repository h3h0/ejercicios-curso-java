/*
 * Copyright (c) 3/18/20
 * Mexico D.F.
 * All rights reserved.
 *
 * THIS SOFTWARE IS CONFIDENTIAL INFORMATION PROPRIETARY OF
 * THIS INFORMATION SHOULD NOT BE DISCLOSED AND MAY ONLY BE USED IN ACCORDANCE THE TERMS DETERMINED BY THE COMPANY ITSELF.
 */
package mx.com.praxis.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import mx.com.praxis.dtos.AutomovilDto;

/**
 * <p></p>
 *
 * @author Marco Acevedo
 * @version Programming
 * @since Programming
 */
@Getter
@Setter
public class Automovil implements Serializable {

    private static final long serialVersionUID = -7235706317471847906L;

    private long id;
    private String placas;
    private String marca;
    private String tipoCombustible;
    private int modelo;
}
