package mx.com.praxis.daos;

import java.util.List;

public interface GenericDao<T, K> {
	List<T> findAll();

	T findById(K key);

	void update(T entity);

	void insert(T entity);
}
