/*
 * Copyright (c) 3/18/20
 * Mexico D.F.
 * All rights reserved.
 *
 * THIS SOFTWARE IS  CONFIDENTIAL INFORMATION PROPRIETARY OF
 * THIS INFORMATION SHOULD NOT BE DISCLOSED AND MAY ONLY BE USED IN ACCORDANCE THE TERMS DETERMINED BY THE COMPANY ITSELF.
 */
package mx.com.praxis.daos;

import mx.com.praxis.entities.Automovil;

/**
 * <p>
 * </p>
 *
 * @author Marco Acevedo
 * @version Programming
 * @since Programming
 */
public interface AutomovilDao extends GenericDao<Automovil, Long> {
	AutomovilDaoImpl automovilDaoImpl = new AutomovilDaoImpl();
	
	Automovil findByPlacasAuto(String Placas);

	static AutomovilDao getInstance(){
		return automovilDaoImpl;
	}
}
