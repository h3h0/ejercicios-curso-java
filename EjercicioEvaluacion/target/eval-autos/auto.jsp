<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Automoviles</title>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap-reboot.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap-grid.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css"/>
</head>

<body>
	<h1>Automoviles</h1>
	<div class="container">
		<div class="row">
			<div class="card shadow mb-4">
				<div class="card-header py-3">
				  <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-plus-circle fa-2x text-gray-300"></i></h6>
				</div>
				<div class="card-body">
					<form action="${pageContext.request.contextPath}/automovil" method="POST">
					  <input type="text" class="form-control form-control-user" id="placas" placeholder="placas" name="placas">
					  <input type="text" class="form-control form-control-user" id="marca" placeholder="marca" name="marca">
					  <input type="text" class="form-control form-control-user" id="tipoCombustible" placeholder="tipoCombustible" name="tipoCombustible">
					  <input type="text" class="form-control form-control-user" id="modelo" placeholder="modelo" name="modelo">
					  <input type="submit" value="Send Request">
					</form>	
				</div>
			</div>
		</div>
		<div class="row">
			Praxis 2020
		</div>
	</div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
</body>

</html>