/*
 * Copyright (c) 3/18/20
 * Mexico D.F.
 * All rights reserved.
 *
 * THIS SOFTWARE IS  CONFIDENTIAL INFORMATION PROPRIETARY OF
 * THIS INFORMATION SHOULD NOT BE DISCLOSED AND MAY ONLY BE USED IN ACCORDANCE THE TERMS DETERMINED BY THE COMPANY ITSELF.
 */
package mx.com.praxis.daos;

import java.util.List;

import javax.ejb.Local;

import mx.com.praxis.daos.impl.PersonDaoImpl;
import mx.com.praxis.entities.Person;

/**
 * <p>
 * </p>
 *
 * @author Marco Acevedo
 * @version Programming
 * @since Programming
 */
@Local
public interface PersonDao extends GenericDao<Person, Long> {
	List<Person> findByFistName(int page, int size, String firstName);
}
