/*
 * Copyright (c) 3/18/20
 * Mexico D.F.
 * All rights reserved.
 *
 * THIS SOFTWARE IS  CONFIDENTIAL INFORMATION PROPRIETARY OF
 * THIS INFORMATION SHOULD NOT BE DISCLOSED AND MAY ONLY BE USED IN ACCORDANCE THE TERMS DETERMINED BY THE COMPANY ITSELF.
 */
package mx.com.praxis.controllers;

import java.io.IOException;
import java.util.Optional;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.praxis.dtos.PersonDto;
import mx.com.praxis.dtos.StudentDto;
import mx.com.praxis.dtos.StudentDto.StudentDtoBuilder;
import mx.com.praxis.services.PersonService;
import mx.com.praxis.services.StudentService;
import mx.com.praxis.utils.Constants;

/**
 * <p>
 * </p>
 *
 * @author Marco Acevedo
 * @version Programming
 * @since Programming
 */
@WebServlet("/students")
public class StudentController extends HttpServlet {
	private static final long serialVersionUID = 4231814006439125032L;

	@Inject
	private StudentService studentService;
	private PersonService personService;

	@Override
	protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {

		final StudentDtoBuilder studentBuilder = StudentDto.builder()
				.universityCareer(request.getParameter("universityCareer"))
				.yearsBachelorDegree(Short.parseShort(request.getParameter("yearsBachelorDegree")));

		if (request.getParameterMap().containsKey(Constants.ID)) {
			final StudentDto student = studentBuilder.id(Long.parseLong(request.getParameter(Constants.ID))).build();

			studentService.update(student);
		} else {
			final StudentDto student = studentBuilder
					.person(PersonDto.builder().id(Long.parseLong(request.getParameter(Constants.ID_PERSON))).build())
					.build();

			studentService.insert(student);
		}

		response.sendRedirect(request.getContextPath() + "/students");
	}

	@Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {

		if (request.getParameterMap().containsKey(Constants.ID)) {
			final long id = Long.parseLong(request.getParameter(Constants.ID));

			request.setAttribute("student", studentService.findById(id));
			request.getRequestDispatcher("/students/detail.jsp").forward(request, response);
		} else {
			final int page = Integer.parseInt(Optional.ofNullable(request.getParameter(Constants.PAGE)).orElse("1"));
			final int size = Integer.parseInt(Optional.ofNullable(request.getParameter(Constants.SIZE)).orElse("10"));

			request.setAttribute("students", studentService.getStudents(page, size));
			request.setAttribute("people", personService.getPeople(1, 100));
			request.getRequestDispatcher("/students/index.jsp").forward(request, response);
		}
	}
}
